import {
    HomePage,
    CategoryPage,
    LoginPage,
    SignupPage,
    CartPage,
    OrdersPage
} from "./pages";

const publicRoutes = [
    {
        path: "/",
        component: HomePage
    },
    {
        path: "/login",
        component: LoginPage
    },
    {
        path: "/signup",
        component: SignupPage
    },
    {
        path: "/category/:ID",
        component: CategoryPage
    },
    {
        path: "/cart",
        component: CartPage
    },
   
]

const privateRoutes = [
    {
        path: "/orders",
        component: OrdersPage
    }
]



//  kada se ima jedna varijabla u file-u preporučeno je da se koristi default export
export default { publicRoutes, privateRoutes };