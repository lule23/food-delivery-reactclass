import React, { createContext, useState } from "react";
import api from "../api/auth";

const initialState = {
    user: null,
    token: localStorage.getItem("auth:token") || null
}

export const AuthContext = createContext();
const AuthContextProvider = ({ children }) => {
    const [state, setState] = useState(initialState)
    const login = async ({  email, password, successCallback, failureCallback }) => {
        try {
            const response = await api.login(email, password);
            setState(prevState => ({
                ...prevState,
                user: response.data.user,
                token: response.data.token
            }));

            localStorage.setItem("auth:token", response.data.token);
            successCallback();
        } catch (error) {
            failureCallback(error.response.data.message);
        }
    }

    const signup = async ({ email, password, telephone, address, successCallback, failureCallback }) => {
        try {
            await api.signup({email, password, telephone, address });
            successCallback();
        } catch (error) {
            failureCallback(error.response.data.message);
        }
    }

    return <AuthContext.Provider value={{ login, signup }}>
        {children}
    </AuthContext.Provider>
}

export default AuthContextProvider;