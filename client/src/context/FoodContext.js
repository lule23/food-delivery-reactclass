import React, { createContext, useState, useEffect } from "react";
import api from "../api/category";
import categoriesApi from "../api/category"

const FoodContext = createContext();

const initialState = {
    categories: [],
    activeCategory: {
        name: "",
        description: "",
        products: []
    },
    cart: []
}

const FoodContextProvider = ({ children }) => {
    const [state, setState] = useState(initialState);

    const getCategories = async () => {
        try {
           const response = await categoriesApi.getCategories();
           setState(prevState => ({
            ...prevState,
            categories: response.data
         }))

        } catch (error) {
            console.log(error.message)
        }
    }

    const getCategory = async (id) => {
        try {
            const response = await api.getCategory(id);
            setState(prevState => ({
                ...prevState,
                activeCategory: response.data
            }))
        } catch (error) {
            console.log(error.message)
        }
    }

    const addProductToCart = product => {
        const newCart = state.cart;
        const existingProductIndex = newCart.findIndex(item => item._id === product._id);

        if(existingProductIndex !== -1) {
            const existingProduct = newCart[existingProductIndex];
            newCart[existingProductIndex] = { ...existingProduct, quantity: existingProduct.quantity + 1};

            return setState(prevState => ({
                ...prevState,
                cart: newCart
            }))
        }

        newCart.push({ ...product, quantity: 1});
        setState(prevState => ({
            ...prevState,
            cart: newCart
        }))
    }

    const removeProductFromCart = id => {
        const newCart = state.cart;

        setState(prevState => ({
            ...prevState,
            cart: newCart.filter(product => product._id !== id)
        }))
    }

    useEffect(() => {
        if(state.categories.length === 0) {
            getCategories();
        }
    }, []);

    return (
        <FoodContext.Provider value={{ ...state, getCategories, getCategory, addProductToCart, removeProductFromCart }}>
             {children}   
        </FoodContext.Provider>
    )
}

export { FoodContext }
export default FoodContextProvider;