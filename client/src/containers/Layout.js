import React from "react";
import { AppBar, Toolbar, Button } from "@material-ui/core";
import { useHistory } from "react-router-dom"

const Layout = ({ children }) => {
    const history = useHistory();

    return (
        <React.Fragment>
            <header>
                <AppBar color="primary" position="sticky">
                    <Toolbar>
                        <Button color="primary" variant="contained">Login</Button>
                        <Button color="secondary" variant="contained" onClick={() => history.push("/cart")}>My cart</Button>
                    </Toolbar>
                </AppBar>
            </header>
            <main>
                {children}
            </main>
            <footer></footer>
        </React.Fragment>
    )
}

export default Layout;
