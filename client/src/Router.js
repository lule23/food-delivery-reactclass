import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import { PrivateRoute } from "./components";
import { Layout } from "./containers"
import routes from "./routes";

/*

Route exact => boolean prop koji naglašava da putanja u URL-u mora 
tačno da se poklopi sa path property-jem koji prima ruta.

*/

const Router = () => {
    return  (
            <BrowserRouter>
                <Layout>
                    <Switch>
                        {
                            routes.publicRoutes.map(route => {
                                return <Route exact path={route.path} component={route.component} />
                            })
                        }
                         {
                            routes.privateRoutes.map(route => {
                                return <PrivateRoute exact path={route.path} component={route.component} />
                            })
                        }
                    </Switch>
                </Layout>
            </BrowserRouter>
    )
}

export default Router;