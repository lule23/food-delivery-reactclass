import axios from "axios";

const api = {
    login: (email, password) => axios.post("/user/login", { email, password }),
    signup: ({ email, password, telephone, address }) => axios.post("/user", { email, password, phone: telephone, address })
}

export default api;