import axios from "axios";

const api = {
    getCategories: () => axios.get("/category"),
    getCategory: id => axios.get(`/category/${id}`)
}

export default api;