import React, { useContext, useEffect } from "react";
import { Link } from "react-router-dom";
import { Typography, Grid } from "@material-ui/core"
import { FoodContext } from "../context/FoodContext"
import { Product } from "../components"

const CategoryPage = ({ history, location, match }) => {
    const categoryID = match.params.ID;
    const { getCategory, activeCategory, categories, addProductToCart } = useContext(FoodContext);

    useEffect(() => {
        getCategory(categoryID);
    }, [categoryID]);

    return (
        <Grid container justify="flex-start" alignItems="flex-start">
         <Grid item xs={12} md={3}>
            <aside style={{ padding: 30 }}>
                {categories.map(category => <Link to={`/category/${category._id}`} style={{ display: "block", marginBottom: "10px" }}>{category.name}</Link>)}
            </aside>
         </Grid>
            <Grid item xs={12} md={9}>
                <div>
                    <Typography variant="h1">{activeCategory.name}</Typography>
                    <Typography variant="body1">{activeCategory.description}</Typography>
                    <Grid container justify="space-between" alignItems="stretch">
                        {activeCategory.products.map(product => {
                            return (
                                <Grid key={product._id} item xs={12} md={3}>
                                    <Product 
                                        name={product.name} 
                                        price={product.price} 
                                        image={product.image} 
                                        onClick={() => addProductToCart(product)}
                                    />
                                </Grid>
                            )
                        })}
                    </Grid>
                </div>
            </Grid>
        </Grid>
    )
}

export default CategoryPage