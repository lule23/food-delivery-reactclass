import React, { useContext } from "react";
import { AuthContext } from "../context/AuthContext";
import { SignupForm } from "../forms"

const SignupPage = ({ history }) => {
    const { signup } = useContext(AuthContext);

    const handleSingup = ({ email, password, addrees, telephone, onFailure }) => {
        const successCallback = () => history.push("/login");

        const failureCallback = (message) => {
            onFailure(message);
            alert("Wrong credentials");
        }

        signup({ email, password, addrees, telephone, successCallback, failureCallback })
    }

    return <SignupForm onSubmit={handleSingup} />
}

export default SignupPage