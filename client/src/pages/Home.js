import React, { useContext } from "react";
import { Link } from "react-router-dom";
import { Card, CardContent, Typography, CardActions } from "@material-ui/core"
import { FoodContext } from "../context/FoodContext"

const HomePage = () => {
    const { categories } = useContext(FoodContext);

    return <div>
        {
           categories.map(category => {
                return (
                    <Card key={category._id}>
                        <CardContent>
                            <Typography variant="h3">{category.name}</Typography>
                        </CardContent>
                        <CardActions>
                            <Link to={`/category/${category._id}`}>More info...</Link>
                        </CardActions>
                    </Card>
                )
            })
        }
    </div>
}

export default HomePage