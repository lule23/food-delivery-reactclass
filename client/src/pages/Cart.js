import React, { useContext } from "react";
import { Grid } from "@material-ui/core";
import { FoodContext } from "../context/FoodContext";
import { Product } from "../components";

const CartPage = () => {
    const { cart, removeProductFromCart } = useContext(FoodContext);
    
    return (
        <Grid container justify="space-evenly" alignItems="flex-start">
            <Grid item xs={12} md={6}>
                {cart.map(product => {
                    return (
                        <Product 
                            name={product.name} 
                            price={product.price} 
                            image={product.image} 
                            buttonLabel="Delete" 
                            quantity={product.quantity} 
                            onClick={() => removeProductFromCart(product._id)}
                        />
                    )
                })}
            </Grid>
            <Grid item xs={12} md={6}>
                
            </Grid>
        </Grid>
    )
}

export default CartPage