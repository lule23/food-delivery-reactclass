//  exportuje defaultni export iz file-a kao named export
export { default as HomePage } from "./Home";
export { default as LoginPage } from "./Login";
export { default as SignupPage } from "./Signup";
export { default as OrdersPage } from "./Orders";
export { default as CartPage } from "./Cart";
export { default as CategoryPage } from "./Category";