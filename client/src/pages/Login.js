import React, { useContext } from "react";
import { AuthContext } from "../context/AuthContext"
import { LoginForm } from "../forms"

const LoginPage = ({ history }) => {
    const { login } = useContext(AuthContext);

    const handleLogin = ({ email, password, onFailure }) => {
        const successCallback = () => history.push("/");

        const failureCallback = (message) => {
            onFailure(message);
            alert("Wrong credentials");
        }

        login({ email, password, successCallback, failureCallback })
    }

    return <LoginForm onSubmit={handleLogin}/>
}

export default LoginPage;