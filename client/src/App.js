import React from "react";
import { Provider } from "react-redux";
import Router from "./Router";
import FoodContext from "./context/FoodContext";
import AuthContext from "./context/AuthContext";

import store from "./store/store";

//  import { calculate, sum } from "./pages/Cart"; => importovanje named exporta

const App = () => {
  return (
    <div className="App">
      <AuthContext>
        <FoodContext>
          <Provider store={store}>
            <Router />
          </Provider>
        </FoodContext>
      </AuthContext>
    </div>
  );
}

export default App;
