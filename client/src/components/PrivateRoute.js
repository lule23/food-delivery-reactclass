import React from "react";
import { Route, Redirect } from "react-router-dom";
import { isAuth } from "../utils/auth"

const PrivateRoute = ({ path, component }) => {
    return isAuth(localStorage.getItem("auth:token")) ? <Route path={path} component={component} /> : <Redirect to="/" />
}

export default PrivateRoute;