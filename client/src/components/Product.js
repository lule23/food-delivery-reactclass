import React from "react";
import { Card, CardContent, CardMedia, CardActions, Typography, Button } from "@material-ui/core";

const Product = ({ name, price, image, onClick, buttonLabel = "Buy", quantity }) => {
    return (
        <Card>
            <CardMedia>
                <img src={image} style={{ width: "100%"}} />
            </CardMedia>
            <CardContent>
                <Typography variant="h5">{name}</Typography>
                <Typography variant="body2">{price}$</Typography>
                {quantity && <Typography variant="caption">{quantity}</Typography>}
            </CardContent>
            <CardActions>
                <Button color="secondary" variant="contained" onClick={onClick}>{buttonLabel}</Button>
            </CardActions>
        </Card>
    )
}

export default Product;