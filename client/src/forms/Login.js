import React from "react";
import { TextField, Button } from "@material-ui/core";
import { useForm, Controller } from "react-hook-form";

const LoginForm = ({ onSubmit }) => {

    const { handleSubmit, register, setError, control } = useForm();

    const handleFormSubbmision = ({ email, password }) => {
        const onFailure = (message) => {
            setError("email", {
                type: "manual",
                message,
                shouldFocus: true 
            })
        }
           

        onSubmit({ email, password, onFailure })
    }

    return <form onSubmit={handleSubmit(handleFormSubbmision)}>
        <Controller placeholder="Email" name="email" as={TextField} control={control} defaultValue="" variant="outlined" /> 
        <Controller placeholder="Password" name="password" as={TextField} control={control} defaultValue="" variant="outlined" type="password" />
        <Button type="submit" variant="contained">Login</Button>
    </form>
}

export default LoginForm;