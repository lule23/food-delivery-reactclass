import React from "react";
import { TextField, Button } from "@material-ui/core";
import { useForm, Controller } from "react-hook-form";

const SignupForm = ({ onSubmit }) => {

    const { handleSubmit, register, setError, control } = useForm();

    const handleFormSubbmision = ({ email, password, address, telephone }) => {
        const onFailure = (message) => {
            setError("email", {
                type: "manual",
                message,
                shouldFocus: true 
            })
        }
           

        onSubmit({ email, password, address, telephone, onFailure })
    }

    return <form onSubmit={handleSubmit(handleFormSubbmision)}>
        <Controller placeholder="Email" name="email" as={TextField} control={control} defaultValue="" variant="outlined" /> 
        <Controller placeholder="Password" name="password" as={TextField} control={control} defaultValue="" variant="outlined" type="password" />
        <Controller placeholder="Address" name="address" as={TextField} control={control} defaultValue="" variant="outlined" /> 
        <Controller placeholder="Telephone" name="telephone" as={TextField} control={control} defaultValue="" variant="outlined" />
        <Button type="submit" variant="contained">Login</Button>
    </form>
}

export default SignupForm;