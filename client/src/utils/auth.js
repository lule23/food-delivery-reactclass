const decode = (token) => {
    if (!token) return null;
    const jwt = token.split('.')[1].replace('-', '+').replace('_', '/');
    return JSON.parse(window.atob(jwt));
  };
  
  const isExpired = (token) => {
    if (!token) return true;
    return new Date().valueOf() / 1000 > token.exp;
  };
  
  const isAuth = (token) => !isExpired(decode(token));
  
  export { isAuth };
  