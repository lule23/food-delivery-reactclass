import { createStore, applyMiddleware, combineReducers, compose } from "redux";
import thunk from "redux-thunk";

import categoriesReducer from "./categories/reducer";
import userReducer from "./user/reducer";

const rootReducer = combineReducers({
    categories: categoriesReducer,
    user: userReducer
})

const store = createStore(rootReducer, compose(applyMiddleware(thunk), window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()));

export default store;