const initalState = null;

const userReducer = (state = initalState, action) => {
    switch(action.type) {
        default:
            return state;
    }
}

export default userReducer;